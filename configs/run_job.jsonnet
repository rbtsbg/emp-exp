{
    "download": false,
    "path_config_download": "./configs/imdb/download/download.imdb.test.jsonnet",

    "train": false,
    "path_config_training": "./configs/snli/train/train.empirical-explainer.jsonnet",

    "explain": false,
    "path_config_explain": "./configs/snli/explain/explain.empirical.jsonnet",

    "evaluate": false,
    "path_config_evaluate": "./configs/snli/evaluate/evaluate.jsonnet",

    "visualize": true,
    "path_config_visualize": "./configs/snli/visualize/visualize.jsonnet",
}

